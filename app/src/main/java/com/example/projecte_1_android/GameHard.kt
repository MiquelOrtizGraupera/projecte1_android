package com.example.projecte_1_android

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Chronometer
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.daimajia.androidanimations.library.Techniques
import com.daimajia.androidanimations.library.YoYo

class GameHard: AppCompatActivity(), View.OnClickListener {
    private lateinit var card1: ImageView
    private lateinit var card2: ImageView
    private lateinit var card3: ImageView
    private lateinit var card4: ImageView
    private lateinit var card5: ImageView
    private lateinit var card6: ImageView
    private lateinit var card7: ImageView
    private lateinit var card8: ImageView

    private lateinit var crono: Chronometer

    private lateinit var exit: ImageView

    private lateinit var viewModel : GameHardviewModel

    private var contadorCartesGirades:Int =0

   override fun onCreate(savedInstanceState: Bundle?){
       super.onCreate(savedInstanceState)
       setContentView(R.layout.activity_game_hard)

       viewModel = ViewModelProvider(this).get(GameHardviewModel::class.java)

       card1 = findViewById(R.id.Hardcard1)
       card2 = findViewById(R.id.Hardcard2)
       card3 = findViewById(R.id.Hardcard3)
       card4 = findViewById(R.id.Hardcard4)
       card5 = findViewById(R.id.Hardcard5)
       card6 = findViewById(R.id.Hardcard6)
       card7 = findViewById(R.id.Hardcard7)
       card8 = findViewById(R.id.Hardcard8)

       card1.setOnClickListener(this)
       card2.setOnClickListener(this)
       card3.setOnClickListener(this)
       card4.setOnClickListener(this)
       card5.setOnClickListener(this)
       card6.setOnClickListener(this)
       card7.setOnClickListener(this)
       card8.setOnClickListener(this)

       crono = findViewById(R.id.chronometer)
       crono.start()

       exit = findViewById(R.id.exitGame)

       exit.setOnClickListener{
           val intentExit = Intent(this, ScoreFiJoc::class.java)
           startActivity(intentExit)
       }
       updateUI()
   }

    override fun onClick(v: View?) {
        when (v) {
            card1 -> {
                girarCarta(0, card1)
                comprobar()
            }
            card2 -> {
                girarCarta(1, card2)
                comprobar()
            }
            card3 -> {
                girarCarta(2, card3)
                comprobar()
            }
            card4 -> {
                girarCarta(3, card4)
                comprobar()
            }
            card5 -> {
                girarCarta(4, card5)
                comprobar()
            }
            card6 -> {
                girarCarta(5, card6)
                comprobar()
            }
            card7 -> {
                girarCarta(6, card7)
                comprobar()
            }
            card8 -> {
                girarCarta(7, card8)
                comprobar()
            }
        }
    }
    private fun girarCarta(idCarta: Int, carta: ImageView) {
        YoYo.with(Techniques.FlipInY)
            .duration(1250)
            .playOn(carta)
        carta.setImageResource(viewModel.girarCarta(idCarta))

    }
    private fun updateUI() {
        card1.setImageResource(viewModel.estatCarta(0))
        card2.setImageResource(viewModel.estatCarta(1))
        card3.setImageResource(viewModel.estatCarta(2))
        card4.setImageResource(viewModel.estatCarta(3))
        card5.setImageResource(viewModel.estatCarta(4))
        card6.setImageResource(viewModel.estatCarta(5))
        card7.setImageResource(viewModel.estatCarta(6))
        card8.setImageResource(viewModel.estatCarta(7))

    }

    private fun comprobar(){
        viewModel.clickTotal++

        var listofCard = arrayListOf<Carta>()

        for (i in 0..7){
            if (viewModel.cartes[i].girada == true && viewModel.cartes[i].guanyada == false){
                listofCard.add(viewModel.cartes[i])
            }
        }
        if (listofCard.size >1){
            if (listofCard[0].resId == listofCard[1].resId){
                when(listofCard[0].id){
                    0 -> card1.isEnabled = false
                    1 -> card2.isEnabled = false
                    2 -> card3.isEnabled = false
                    3 -> card4.isEnabled = false
                    4 -> card5.isEnabled = false
                    5 -> card6.isEnabled = false
                    6 -> card7.isEnabled = false
                    7 -> card8.isEnabled = false
                }
                when(listofCard[1].id){
                    0 -> card1.isEnabled = false
                    1 -> card2.isEnabled = false
                    2 -> card3.isEnabled = false
                    3 -> card4.isEnabled = false
                    4 -> card5.isEnabled = false
                    5 -> card6.isEnabled = false
                    6 -> card7.isEnabled = false
                    7 -> card8.isEnabled = false
                }
                contadorCartesGirades = contadorCartesGirades + 2
                listofCard[0].guanyada = true
                listofCard[1].guanyada = true
                if(contadorCartesGirades == 8){
                    fiJoc()
                }
            }else{
                if(listofCard[0].guanyada == false && listofCard[1].guanyada == false){
                    listofCard[0].girada
                    listofCard[1].girada
                    when(listofCard[0].id){
                        0 -> girarCarta(listofCard[0].id,card1)
                        1 -> girarCarta(listofCard[0].id,card2)
                        2 -> girarCarta(listofCard[0].id,card3)
                        3 -> girarCarta(listofCard[0].id,card4)
                        4 -> girarCarta(listofCard[0].id,card5)
                        5 -> girarCarta(listofCard[0].id,card6)
                        6 -> girarCarta(listofCard[0].id,card7)
                        7 -> girarCarta(listofCard[0].id,card8)
                    }
                    when(listofCard[1].id){
                        0 -> girarCarta(listofCard[1].id,card1)
                        1 -> girarCarta(listofCard[1].id,card2)
                        2 -> girarCarta(listofCard[1].id,card3)
                        3 -> girarCarta(listofCard[1].id,card4)
                        4 -> girarCarta(listofCard[1].id,card5)
                        5 -> girarCarta(listofCard[1].id,card6)
                        6 -> girarCarta(listofCard[1].id,card7)
                        7 -> girarCarta(listofCard[1].id,card8)
                    }
                }
            }
            listofCard.clear()
        }
    }
    private fun fiJoc(){
        val finalJoc = Intent(this, ScoreFiJoc::class.java)
        finalJoc.putExtra("Score", viewModel.puntuacio())
        startActivity(finalJoc)
    }
}

