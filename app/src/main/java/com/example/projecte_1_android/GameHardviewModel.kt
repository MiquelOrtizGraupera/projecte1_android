package com.example.projecte_1_android

import androidx.lifecycle.ViewModel

class GameHardviewModel: ViewModel() {
    var imatges = arrayOf(
        R.drawable.apple,
        R.drawable.kiwi,
        R.drawable.koala,
        R.drawable.boxing,
        R.drawable.apple,
        R.drawable.kiwi,
        R.drawable.koala,
        R.drawable.boxing
    )
    var cartes = mutableListOf<Carta>()

    var clickTotal:Int = 0

    // Aquesta funció es estàndard de cada classe, i és com el constructor

    init {
        setDataModel()
    }
    // Funció que barreja les imatges de les cartes i crea l'array de Cartes
    private fun setDataModel() {
        imatges.shuffle()
        for (i in 0..7) {
            cartes.add(Carta(i, imatges[i]))
        }
    }
    // Funció que comprova l'estat de la carta, el canvia i envia
    // a la vista la imatge que s'ha de pintar
    fun girarCarta(idCarta: Int): Int {
        if (!cartes[idCarta].girada) {
            cartes[idCarta].girada = true
            return cartes[idCarta].resId

        } else {
            cartes[idCarta].girada = false
            return R.drawable.x_icon
        }
    }
    // Funció que retorna l'estat actual d'una carta
    fun estatCarta(idCarta: Int): Int {
        if (cartes[idCarta].girada) {
            return cartes[idCarta].resId
        } else return R.drawable.x_icon
    }

    fun puntuacio():Int {
        return 100 - (clickTotal - 8) * 10
    }
}