package com.example.projecte_1_android

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.Spinner

class Menu : AppCompatActivity(), AdapterView.OnItemSelectedListener {
    private lateinit var playButton : Button
    private lateinit var exitButton: Button
    private lateinit var spinner: Spinner

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.SplashTheme)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu)

        playButton = findViewById(R.id.play)
        exitButton = findViewById(R.id.exit)
        spinner = findViewById(R.id.spinner1)

        spinner.adapter = ArrayAdapter.createFromResource(
            this,
            R.array.dificulty,
            android.R.layout.simple_spinner_item
        )

        playButton.setOnClickListener{

            if(spinner.selectedItemPosition == 0) {
                //intent easy
                val intentGame = Intent(this, Game::class.java)
                startActivity(intentGame)
            }else //if(spinner.selectedItemPosition == 1)
                {
                //intent hard
                val intentHardGame = Intent(this, GameHard::class.java)
                startActivity(intentHardGame)
            }
        }

        exitButton.setOnClickListener{
            val intentExit = Intent(this, ScoreFiJoc::class.java)
            startActivity(intentExit)
        }
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        TODO("Not yet implemented")
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {
        TODO("Not yet implemented")
    }
}