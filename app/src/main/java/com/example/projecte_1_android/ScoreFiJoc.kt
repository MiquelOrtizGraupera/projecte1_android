package com.example.projecte_1_android

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import androidx.lifecycle.ViewModelProvider

class ScoreFiJoc : AppCompatActivity() {
    lateinit var playAgain : Button
    lateinit var exitButton : Button
    lateinit var score:TextView
    private lateinit var viewModel : GameviewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_score_fi_joc)


        playAgain = findViewById(R.id.playAgainButton)
        exitButton = findViewById(R.id.exitBUtton)
        score = findViewById(R.id.points)

        playAgain.setOnClickListener{
            val intentPlay = Intent(this, Game::class.java)
            startActivity(intentPlay)
        }

        exitButton.setOnClickListener{
            val intentExit = Intent(this, Menu::class.java)
            startActivity(intentExit)
        }

        val bundle: Bundle? = intent.extras
        val result = bundle?.getInt("Score")

       score.text = (result.toString())


    }


}